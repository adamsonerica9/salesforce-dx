# SFDX Simple App

<!--[![Deploy](https://deploy-to-sfdx.com/dist/assets/images/DeployToSFDX.svg)](https://deploy-to-sfdx.com/)-->

This is a Salesforce Developer Experience (SFDX) sample project forked from forcedotcom/sfdx-simple.

The `.gitlab-ci.yaml` file is pulled from
https://gitlab.com/mayanktahil/sfdx-gitlab-package. We need to set up JWT for
authentication in order to run it.

## Setup


- Make sure that you have Salesforce CLI installed. Run `sfdx force --help` and
  confirm you see the command output. If you don't have it installed, you can
  download and install it from
  [here](https://developer.salesforce.com/tools/sfdxcli).

- Set up a JWT-based auth flow for the target orgs that you want to deploy to. This step creates a `server.key` file that is used in subsequent steps.
(https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_auth_jwt_flow.htm)

- From the above process, retrieve the generated `Consumer Key`.

- _Generate Server.crt and make sure you [`manage your app`](https://developer.salesforce.com/forums/?id=906F0000000AnL3IAK) to add the right profiles to the app. Make sure you note down your client ID when you create a new app._ ???

- To access and login to your dashboard `sfdx force:auth:web:login -d -a DevHub`

- Confirm that you can perform a JWT-based auth: `sfdx force:auth:jwt:grant --clientid <your_consumer_key> --jwtkeyfile server.key --username <your_username> --setdefaultdevhubusername`

- Set up GitLab CI/CD [environment variables](https://gitlab.com/help/ci/variables/README#variables) for your Salesforce `Consumer Key` and `Username`. Note that this username is the username that you use to access your Salesforce org.
    - Create an environment variable named `SF_CONSUMER_KEY` and set it as protected.
    - Create an environment variable named `SF_USERNAME` and set it as protected.
    - **Note:** Setting the variables as protected requires that you set the branch to protected as well.

- Encrypt the generated `server.key` file and add the encrypted file (`server.key.enc`) to the folder named `assets`.

    `openssl aes-256-cbc -salt -e -in server.key -out server.key.enc -k password`

- Set up GitLab CI/CD [environment variable](https://gitlab.com/help/ci/variables/README#variables) for the password you used to encrypt your `server.key` file.

    Create an environment variable named `SERVER_KEY_PASSWORD` and set it as protected.

- Create the sample package. This will update the `sfdx-package.json` file.

    `sfdx force:package:create --path force-app/main/default/ --name "GitLab CI" --description "GitLab CI Package Example" --packagetype Unlocked`

- Create the first package version.

    `sfdx force:package:version:create --package "GitLab CI" --installationkeybypass --wait 10 --json --targetdevhubusername DevHub`

- In the `.gitlab-ci.yml` file, update the value in the `PACKAGENAME` variable to be Package ID in your `sfdx-project.json` file.  This ID starts with `0Ho`.

- Commit the updated `sfdx-project.json` and `.gitlab-ci.yml` files.

### Prior instructions

Authorize to your Developer Hub (Dev Hub) org.

    sfdx force:auth:web:login -d -a "Hub Org"

If you already have an authorized Dev Hub, set it as the default:

    sfdx force:config:set defaultdevhubusername=<username|alias>

Create a scratch org.

    sfdx force:org:create -s -f config/project-scratch-def.json

If you want to use an existing scratch org, set it as the default:

    sfdx force:config:set defaultusername=<username|alias>

Push your source.

    sfdx force:source:push

Run your tests.

    sfdx force:apex:test:run
    sfdx force:apex:test:report -i <id>

Open the scratch org.

    sfdx force:org:open --path one/one.app

## Resources

For details on using this project, please review the [Salesforce DX Developer Guide](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev).

## Description of Files and Directories  

* **sfdx-project.json**: Required by Salesforce DX. Configures your project.  Use this file to specify the parameters that affect your Salesforce development project.
* **config/project-scratch-def.json**: Sample file that shows how to define the shape of a scratch org.  You reference this file when you create your scratch org with the force:org:create command.   
* **force-app**: Directory that contains the source for the sample Force.com app and tests.   
* **.project**:  Required by the Eclipse IDE.  Describes the Eclipse project.
* **.gitignore**:  Optional Git file. Specifies intentionally untracked files that you want Git (or in this case GitHub) to ignore.
